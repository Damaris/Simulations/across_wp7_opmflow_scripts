# ACROSS WP 7.3 Environment Scripts and Build files
  
Holds scripts for sharing environments and running test cases for the Across work package 7.3

## Shared Environment using Singularity 

Singularity is a Linux based container system for software reporducability, somewhat like Docker, however with a  
security profile and capability that suits managed High Performance Computing facilities.
  
[Singularity user guide](https://sylabs.io/guides/3.7/user-guide/index.html)  
[Singularity admin guide](https://sylabs.io/guides/3.7/admin-guide/installation.html)  
  
Singularity requires *sudo* privlages to install the singularity executable and also *sudo* privlages for building images.  
Singularity also requires and a ```Go``` compiler and other generally available packages on common Linux distributions.  
Please follow directions for installation here [admin-guide/installation.html](https://sylabs.io/guides/3.7/admin-guide/installation.html)  
   
Once installed, the scripts in this repository at ./singularty/*.def can be used to obtain   
the required prerequisite libraries (for both OPM and Damaris) and install the combined stack into a  
directory, using the ```singularity build --sandbox``` command. There are source builds (srcdeps) and binary build (bindeps) options  
for installing Damaris dependencies.  
  
N.B. Damaris cannot use the binary paraview-dev package available on Ubuntu 20.04. So requires Paraview to be compiled and installed.  
     The build time for the container that installs Paraview is over 1 hour. The containers end up between 5 to 10 GB (uncompressed)  

```
git clone https://gitlab.inria.fr/Damaris/Simulations/across_wp7_opmflow_scripts.git

# Copy the container definition file (there are directories for Ubuntu 20.04 and CentOS7 base images)
cp across_wp7_opmflow_scripts/singularity/ubuntu/opm_damaris_final_noparaview.def .

# For the CentOS7 definitions also copy the shell variable file 
# (check the %files section if it is used in the build procedure)
cp across_wp7_opmflow_scripts/singularity/centos7/opmbuildstack/opm_stack_build_vars.sh .
# Modify the variables in opm_stack_build_vars.sh to suite your requirements
# i.e. Debug | Release etc. which MPI and where, Dune and OPM tag versions

# build the image (--sandbox means it will be writable):
sudo singularity build --sandbox opm_container opm_damaris_final_noparaview.def 
# <... wait a long time ...>
ls opm_container
# <should see a root filesystem setup>
# Run the container
singularity shell opm_container
# N.B. Your current directory and home directory will be accessible automatically unless you specify ```--containall```
# Check if flow executable is available
which flow
# exit from container
exit
# Get the OMP Flow testing data sets
wget https://github.com/OPM/opm-data/archive/master.zip
unzip master.zip
# Run the container again
singularity shell opm_container
# run flow executable in the container with downloaded dataset
flow --output-dir=./output ./opm-data-master/norne/NORNE_ATW2013.data
# <Should see flow outptut here>
# After simulation has finished, data should be available in pwd
ls ./output

# Or run flow in the container using MPI on the host
mpirun -np 4 singularity exec ./opm_container /usr/local/bin/flow --output-dir=./output ./opm-data-master/norne/NORNE_ATW2013.data
```

### Running a Damaris example using a built container

If you have gone to the effort of building a Paraview enabled version of one of the containers then  
you may want to try to run an example. You will need:  
  * A Paraview GUI installed on the localhost 
  * A matching MPI runtime on your host as what is in the container 
    I manged to run the example on a Debian 10 host after installing ```sudo apt-get install libopenmpi-dev```  
  
N.B. The Paraview version built in the singularity container must match the one on the host, 
     i.e. v5.8.0 of Paraview),  available from: [https://www.paraview.org/download](https://www.paraview.org/download)  
     Download and unpack: ```ParaView-5.8.0-MPI-Linux-Python3.7-64bit.tar.gz```

```
# build the container
sudo singularity build --sandbox opm_container opm_damaris_bindeps_pvsrc.def

# Now enter the container and edit the damaris xml configuration file 
sudo singularity shell --writable  opm_container
nano ${INSTALL_PATH}/examples/paraview/image.xml

# This is probably not needed as the xml files are automaticaly set up to use 
# the ${INSTALL_PATH} value by the build definition file 
# (look for find / sed after damaris install section in the def files)
# find the <script> </script> tags and set the path to the 
# file path as ${INSTALL_PATH}examples/paraview/image.py and save.

# Exit from the container
exit

# run the example:
mpirun -np 4 singularity exec ./centos7base_2c /usr/local/examples/paraview/image /usr/local/examples/paraview/image.xml

# Launch Paraview on your host PC and select 'catalyst' menu and select 'connect' and 'ok' to default port of 22222
# Then click on the input 'triangle' icon and then the 'eye' icon and set up the 'Properties' tab to display things of interest.

```
  
## Spack Environemnt for OPM Flow and Damaris
TODO
[spack](https://spack.readthedocs.io/en/latest/)



