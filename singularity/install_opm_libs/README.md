## Installing Dune and OPM Flow from source

The Singulatiry build file ```opm_damaris_bindeps_nopv_dune_srcbld_ubu21_ipo.def``` 
gives an example of  compiling OPM Flow and it's Dune dependencies from source and 
with Interprocedural Optimization (IPO) enabled. 
 
## ARM Map profiling
The file ```opm_damaris_bindeps_nopv_dune_srcbld_ubu21_ipo.def```  also gives an 
example of running the build execuateble with the ARM Map profiler.

## Bash helper scrits for OPM Flow
File ```get_opm_libs_github.sh``` provides some helper functions for updating OMP repositories
and installing Dune from pre-compiled Ubuntu repoisitories.