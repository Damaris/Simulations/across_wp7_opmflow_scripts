#! /bin/bash
# Functions to download or update OPM repositories
# Use: 
#   >update_all_opm /home/josh/opmbuild
#   Install dune libs on Ubuntu from repository
#   >install_dune 
#   >opm_build_repos /home/josh/opmbuild Debug  /home/josh/local
#
# Caution: This script deletes and the recreates the $basedir/build/$repo e.g. /home/josh/opmbuild/build/opm-simulators
   

install_opm () {
  basedir=$(realpath $1)
  repo=$2
  github=$3
  branch=$4
  
  echo "basedir=$basedir "
  echo "repo   =$repo    "
  echo "github =$github  "
  echo "branch =$branch  "
  # return 
  
  if [[ -d $basedir ]] ; then
    cd $basedir
  else
    echo "ERROR: 1st argument should be the base directory and it should exist: $basedir"
    return
  fi
  
  # Check if $repo directory exists and pull the latest version of $branch
  if [[ -d $repo ]] ; then
    cd $repo
    git fetch --all
    git checkout $branch
    git pull    
  else  
    # directory did not exist so clone the $repo and checkout the correct $branch
    git clone https://$github/$repo.git
    if [[  "$branch" != "master" ]] ; then
      cd $repo
      git fetch --all
      git checkout $branch
    fi 
  fi 
  cd ..
  
  # make sure our variable is not empty
  if [[ ! -z $repo ]] ; then
    if [[ -d $basedir/build/$repo ]] ; then
      echo "INFO: removing build directory and then creating again: $basedir/build/$repo"
      rm -fr $basedir/build/$repo
      mkdir -p  $basedir/build/$repo
    else
      echo "INFO: creating build directory: $basedir/build/$repo"
      mkdir -p $basedir/build/$repo
    fi
  fi 
  
  cd $basedir/build/$repo
}    
    
    
    
update_all_opm () {
  basedir=$(realpath $1)
  install_opm $basedir opm-common github.com/OPM master
  install_opm $basedir opm-material github.com/OPM master
  install_opm $basedir opm-grid github.com/OPM master
  install_opm $basedir opm-models github.com/OPM master
  install_opm $basedir opm-simulators github.com/OPM master
  install_opm $basedir opm-upscaling github.com/OPM master
  install_opm $basedir opm-data github.com/OPM master
    
}



install_dune () {

    REQUERIMENTS="xsdcxx libhdf5-openmpi-dev  hdf5-tools libopenblas-dev \
    libsuitesparse-dev libtrilinos-zoltan-dev \
    libdune-common-dev libdune-geometry-dev \
    libdune-istl-dev libdune-grid-dev"

    sudo  apt-get install -y $REQUERIMENTS
    if [[ ! -f /usr/bin/xsd ]] ; then 
      sudo ln -s /usr/bin/xsdcxx /usr/bin/xsd
      # or : cp /usr/bin/xsdxx /usr/bin/xsd
    fi
}


opm_build_repos () {
    basedir=$1
    BT=$2
    INSTALL_PREFIX=$3
    NUMCORES=8

    export LD_LIBRARY_PATH=$INSTALL_PREFIX/lib:$LD_LIBRARY_PATH
    export PATH=$INSTALL_PREFIX/bin:$PATH

    cd ${basedir}/build/opm-common
    cmake ../../opm-common -DCMAKE_INSTALL_PREFIX:PATH=$INSTALL_PREFIX -DUSE_MPI=ON  -DCMAKE_BUILD_TYPE:STRING=${BT} -DBUILD_TESTING=OFF -DBUILD_EXAMPLES=OFF
    make -j${NUMCORES} && make install
    make clean
    
    cd ${basedir}/build/opm-material
    cmake ../../opm-material -DCMAKE_INSTALL_PREFIX:PATH=$INSTALL_PREFIX -DUSE_MPI=ON  -DCMAKE_BUILD_TYPE:STRING=${BT} -DBUILD_TESTING=OFF -DBUILD_EXAMPLES=OFF
    make -j${NUMCORES} && make install
    make clean
    
    cd ${basedir}/build/opm-grid
    cmake ../../opm-grid -DCMAKE_INSTALL_PREFIX:PATH=$INSTALL_PREFIX -DUSE_MPI=ON  -DCMAKE_BUILD_TYPE:STRING=${BT} -DBUILD_TESTING=OFF -DBUILD_EXAMPLES=OFF
    make -j${NUMCORES} && make install
    make clean

    
    cd ${basedir}/build/opm-models
    cmake ../../opm-models -DCMAKE_INSTALL_PREFIX:PATH=$INSTALL_PREFIX -DUSE_MPI=ON -DCMAKE_BUILD_TYPE:STRING=${BT} -DBUILD_TESTING=OFF -DBUILD_EXAMPLES=OFF
    make -j${NUMCORES} && make install
    make clean
    
    
    cd ${basedir}/build/opm-simulators
    # If Damaris is in an unusual place then use -DCMAKE_PREFIX_PATH=/path/to/rootdir
    cmake ../../opm-simulators -DUSE_DAMARIS_LIB=ON -DBUILD_TESTING=OFF -DBUILD_EXAMPLES=OFF -DCMAKE_INSTALL_PREFIX:PATH=$INSTALL_PREFIX -DUSE_MPI=ON -DCMAKE_BUILD_TYPE:STRING=${BT} -DCMAKE_CXX_FLAGS="-Wno-shadow -Wno-cast-function-type" -DCMAKE_DISABLE_FIND_PACKAGE_CUDA=1 -DCMAKE_DISABLE_FIND_PACKAGE_OpenCL=1
    make -j${NUMCORES} && make install
    make clean
   
}

