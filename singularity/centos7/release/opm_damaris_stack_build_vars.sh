#! /bin/bash
# File:   opm_damaris_stack_build_vars.sh
# Author: Josh Bowden, Inria
# Date:   22/04/2021
# 
# This file is to be copied in to the  and sourced by singularity build definition files
# e.g.
# source /opmbuildstack/opm_damaris_stack_build_vars.sh
# 

# Build with this many cores
NUMCORES=4

# Enable the devtools environment 
# The number implies a GCC version
# DEVTOOLSET=devtoolset-9
# Installed using:
# yum -y install centos-release-scl
# yum -y install ${DEVTOOLSET}
#Enable the devtools environment 
# scl enable ${DEVTOOLSET} -- bash

# OMPBUILDSTACKDIR=/opmbuildstackdir

export GCC=/opt/rh/${DEVTOOLSET}/root/usr/bin/gcc
export GPP=/opt/rh/${DEVTOOLSET}/root/usr/bin/g++
export CC=$GCC
export CXX=$GPP
export CMAKE_C_COMPILER=$GCC
export CMAKE_CXX_COMPILER=$GPP
    
    
# The file is usefull to keep all variables consistent over multi-definition builds
 # Options: Debug | Release | MinSizeRel | RelWithDebInfo
CMAKE_BUILD_TYPE=Release
INSTALL_PREFIX=/usr/local
# Directory where all libraries will be downloaded to and built from


SUPERLU_BLAS="-L/usr/lib64 -lopenblas"
BLAS_LIB="-L/usr/lib64 -lopenblas"
LAPACK_LIB="-L/usr/lib64 -lopenblas"

#  Set compiler specific optimisation flags
CMAKE_CXX_FLAGS="-march=native"
CMAKE_C_FLAGS="-march=native"
CFLAGS_BUILD="-O3 -march=native"
CXXFLAGS_BUILD="-O3 -march=native" 
export CFLAGS=${CFLAGS_BUILD}
export CXXFLAGS=${CFLAGS_BUILD}

# OpenMPI 4.1.0 fails if -march=native is added 
CFLAGS_OMPI="-O3 "
CXXFLAGS_OMPI="-O3 "

# SuiteSparse
OPTIMIZATION="-O3 -march=native"
## Target CPU architectures example supported values are: 
## "none", "generic", "core","sandy-bridge", "ivy-bridge", "haswell", "broadwell", "skylake", "skylake-xeon", "kaby-lake", "cannonlake", "silvermont", "goldmont", "knl" (Knights Landing), "magny-cours", "bulldozer", "interlagos", "piledriver", "AMD 14h", "AMD 16h", "zen"   
TA="-DTARGET_ARCHITECTURE:STRING=auto"
OPENMPLIB="-fopenmp"
BUILD_SHARED="-DBUILD_SHARED_LIBS=TRUE"
BUILD_TESTING="-DBUILD_TESTING=FALSE"

# MPI paths
OPENMPI_VERSION=4.1.0
OPENMPI_VER_MAJ_MIN=4.1
MPI_BINDIR=/usr/local/bin
MPI_LIBDIR=/usr/local/lib
MPI_INCDIR=/usr/local/include

# INSTALL_PREFIX=/usr
export PATH=$INSTALL_PREFIX/bin:$PATH
export PATH=${MPI_BINDIR}:$PATH
export LD_LIBRARY_PATH=$INSTALL_PREFIX/lib:$INSTALL_PREFIX/lib64:$MPI_LIBDIR:$LD_LIBRARY_PATH

# Tag values for git checkouts
SUPERLUTAG=v5.2.2
SUITESPARSETAG=v5.9.0
DUNETAG=v2.6.0
OPMTAG=release/2020.10
SCOTCH_TAG=v6.1.0
VC_TAG=1.4.1



# Update cmake on centos7
CMAKE_VERSION=3.16
CMAKE_VERSION_FULL=${CMAKE_VERSION}.3

# Damaris dependeciy library versions
DAMARIS_VERSION=1.3.3
PV_VERSION=5.8.0
BOOST_VERSION_MAJ=1
BOOST_VERSION_MIN=67
BOOST_VERSION_PATCH=0
XERCES_VERSION=3.2.2
XSD_VERSION=4.0.0-1
# CPPUNIT_VERSION=1.12.1
# 1.8.12  (CentOS7 vwersion
# HDFVERSION=1.12
# HDFVERPOINT=.0
HDFVERSION=1.10
HDFVERPOINT=.7


# PYTHON_INCLUDE_DIR=/usr/include/python3.8  
PYTHON_INCLUDE_DIR=/usr/include/python3.6m
GL_INSTALL_PATH=/usr/lib64 
GL_HEADER_PATH=/usr
# Build damaris as shared library (N.B. static library build has problems)
SHLIBS_ON_OFF=ON
# Build examples
EXAMPLES_ON_OFF=ON
# Regenerate the xml model in C using XSD
REGEN_ON_OFF=ON
