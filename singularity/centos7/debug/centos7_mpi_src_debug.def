Bootstrap: docker
From: centos:7
Stage: build

%help
    This container was built from the following repositpory of Singularity definition files:
        git clone https://gitlab.inria.fr/Damaris/Simulations/across_wp7_opmflow_scripts.git
        
    This version builds CentOS7 with updated gcc via RedHat devtoolset, Set DEVTOOLSET in both
    the %post and %environment sections below. Initialise the toolset using:
        > scl enable ${DEVTOOLSET} -- bash
        
    Most dependency versions can be define in : opm_damaris_stack_build_vars.sh which will
    be copied into the container in the first build step and sourced so as to get the version 
    numbers and install paths etc. Please modify the variables to update or modify the 
    exact dependencies wanted.

    |     | devtoolset | xerces-c | xsd   |  HDF5       |  UCX   |    mpi        | Paraview |
    |-----|------------|----------|-------|-------------|--------|---------------|----------|
    | src |            |          |       |             | 1.10.0 | OpenMPI 4.1.0 |          |
    | bin |  9         | 3.1.1    | 4.0.0 |             |        |               |          |
    
    This version installs:
    
    DUNE git libraries from source (checkout version DUNETAG=v2.6.0)
    - libdune-common
    - libdune-geometry
    - libdune-istl   (apt dependencies libsuitespares-dev libsuperlu-dev)
    - libdune-uggrid-dev 
    - libdune-grid   (apt dependencies libalberta-dev)
    
    And OPM libraries from git src
    (checkout versionOPMTAG=release/2020.10)
    
    
    To build a contianer from a definition file:
        # Copy input variable file that will be sourced within the container
        # at the start of a build.
         cp across_wp7_opmflow_scripts/singularity/centos7/opm_damaris_stack_build_vars_debug.sh .
        # set the build variables in opm_damaris_stack_build_vars.sh to suite
        # Copy the definition files
        cp across_wp7_opmflow_scripts/singularity/centos7/centos7_mpi_src_debug.def .
        cp across_wp7_opmflow_scripts/singularity/centos7/opm_dune_mpi_srcbld_centos_debug_p2a.def .
        cp across_wp7_opmflow_scripts/singularity/centos7/opm_dune_mpi_srcbld_centos_debug_p2b.def .
        cp across_wp7_opmflow_scripts/singularity/centos7/opm_dune_mpi_srcbld_centos_debug_p2c.def .
      It now is a 4 stage build process:  - CentOS7 is old so we need to compile many again from source
        # step 1: Install some basic pre-packaged libraries - CentOS7 is old so we need to compile many again from source
        # also builds UCX and OpenMPI 4.1.0
        sudo singularity build --sandbox ./centos7debug centos7_mpi_src_debug.def
        # step 2: build Dune/OPM dependencies from source ( SuiteSparse SuperLU Vc TBB Scotch Zoltan etc.)     
        sudo singularity build --sandbox centos7debug_2a opm_dune_mpi_srcbld_centos_debug_p2a.def
        # step 3: build Damaris dependencies (Optional: HDF5, Paraview) and Damaris 
         sudo singularity build --sandbox centos7debug_2b opm_dune_mpi_srcbld_centos_debug_p2b.def
        # step 4: build Dune and OPM libraries  
        sudo singularity build --sandbox centos7debug_2c opm_dune_mpi_srcbld_centos_debug_p2c.def
        
    To get a shell into the container:
        sudo singularity shell --writable centos7debug_2c
                
    To locate the source repositories for dependencies and Damaris and OPM software:
        cd ${OMPBUILDSTACKDIR} # /opmbuildstackdir
        
%setup

%files
    # damaris-development-code_saturne.tar.gz /builddir/damaris-development-code_saturne.tar.gz
    # cppunit-1.12.1.tar.gz /builddir/cppunit-1.12.1.tar.gz
    
    # Copy in the environment variable files - the directory will be created and is also to
    # be matched within the file with variable OMPBUILDSTACKDIR=/opmbuildstackdir 
    opm_damaris_stack_build_vars.sh /opmbuildstackdir/opm_stack_build_vars.sh
    boost_1_67_0.tar.gz /opmbuildstackdir/boost_1_67_0.tar.gz
    hdf5-1.12.0.tar.gz /opmbuildstackdir/hdf5-1.12.0.tar.gz
    mpfr-4.1.0.tar.gz /opmbuildstackdir/mpfr-4.1.0.tar.gz
    openmpi-4.1.0.tar.gz /opmbuildstackdir/openmpi-4.1.0.tar.gz
    ucx-1.10.0.tar.gz /opmbuildstackdir/ucx-1.10.0.tar.gz
    xerces-c-3.2.2.tar.gz /opmbuildstackdir/xerces-c-3.2.2.tar.gz
    cmake-3.16.3-linux-x86_64.tar.gz  /opmbuildstackdir/cmake-3.16.3-linux-x86_64.tar.gz

%environment
    export LC_ALL=C
    export DEBIAN_FRONTEND=noninteractive  
    # INSTALL_PREFIX should match value used in post section

    
    # N.B. both these variables should match what are in "opm_damaris_stack_build_vars.sh"
    export DEVTOOLSET=devtoolset-9
    export OMPBUILDSTACKDIR=/opmbuildstackdir
    
    
    export INSTALL_PREFIX=/usr/local
    export PATH=$INSTALL_PREFIX/bin:$PATH
    export LD_LIBRARY_PATH=$INSTALL_PREFIX/lib:$INSTALL_PREFIX/lib64:$LD_LIBRARY_PATH

%post

    ############
    ### Install newer gcc version 
    ############
    
    # These two must be specified in the %environment section also
    export DEVTOOLSET=devtoolset-9
    export OMPBUILDSTACKDIR=/opmbuildstackdir
    
    # source ${OMPBUILDSTACKDIR}/opm_stack_build_vars.sh
    yum -y install centos-release-scl
    yum -y install ${DEVTOOLSET}
     
     
    ############
    ### Shell variables for install paths, optimisation levels, software versions
    ############ 
    #### Enable the gcc version in the devtools environment 
    scl enable ${DEVTOOLSET} -- bash

    # Source the variables to be used
    source ${OMPBUILDSTACKDIR}/opm_stack_build_vars.sh
    

    ############
    ### Start the binary package installs
    ############
    
    yum -y update
    cat /etc/redhat-release

    # yum -y group install "Development Tools"
    yum -y install m4 bison openssl-devel git wget unzip nano boost-devel fmt-devel 


    # OpenBLAS is only available from the EPEL repo
    yum -y install epel-release
    yum repolist
    yum -y install openblas-devel python3-devel
    yum -y install texlive ghostscript
        
    # Removing these as they cause an OPM build to lock up when building docs (on master braches)
    # doxygen graphviz gnuplot
        
    ### Installed from source : suitesparse superlu zoltan ptscotch tbb
    # Optional: libalberta-dev 
    # Used by OPM: libtrilinos-zoltan-dev   

    ### Dependencies of SuiteSparse: gmp-devel mpfr-devel Installed from source in srcbld_centos_p2a

    ###  DUNE pre-packaged libraries are available but are being install from source below
    # tbb-devel
    # apt-get -y install  libdune-common-dev libdune-geometry-dev libdune-istl-dev libdune-grid-dev


    ############
    ## Setup MPI
    ############
    ## For Intel MPI see: https://software.intel.com/content/www/us/en/develop/documentation/mpi-developer-guide-linux/top/running-applications/running-intel-mpi-library-in-containers/run-the-application-with-a-container.html

    #####
    ##  OpenMPI
    #####
    ### Here is the MPI version and a HDF with parallel i/o capabilities
    ### HDF
    # yum -y install openmpi-devel hdf5-openmpi-devel  

    #####
    ##  MPICH MPI - use MPICH for IntelMPI compatibility
    #####
    # yum -y install mpich-3.0-devel hdf5-mpich-devel

    ####
    ##  For Damaris (in addition to mpi and hdf)
    ####
    yum -y install xerces-c-devel 
    
    #### 
    ## This directory can be made in the files section above
    INSTALLTMDIR=${OMPBUILDSTACKDIR}
    # mkdir ${INSTALLTMDIR}

    # yum -y erase openmpi 

    ###### Install CMake - from source (slow)
    # cd ${INSTALLTMDIR}
    # wget https://cmake.org/files/v${CMAKE_VERSION}/cmake-${CMAKE_VERSION_FULL}.tar.gz
    # tar -zxvf cmake-${CMAKE_VERSION_FULL}.tar.gz
    # cd cmake-${CMAKE_VERSION_FULL}
    # ./bootstrap -- -DBUILD_TESTING=OFF
    # make
    # make install

    cd ${INSTALLTMDIR}
    ###### Install CMake - from binary (fast)
    echo "CMake Version: ${CMAKE_VERSION_FULL}"
    if [[ ! -f  ${INSTALLTMDIR}/cmake-${CMAKE_VERSION_FULL}-linux-x86_64.tar.gz ]] ; then 
      wget https://github.com/Kitware/CMake/releases/download/v${CMAKE_VERSION_FULL}/cmake-${CMAKE_VERSION_FULL}-linux-x86_64.tar.gz
    fi
    tar -xf cmake-${CMAKE_VERSION_FULL}-linux-x86_64.tar.gz
    # rm cmake-${CMAKE_VERSION_FULL}-linux-x86_64.tar.gz
    cp -r cmake-${CMAKE_VERSION_FULL}-Linux-x86_64/bin /usr/local
    cp -r cmake-${CMAKE_VERSION_FULL}-Linux-x86_64/share/cmake-${CMAKE_VERSION} /usr/local/share


    cd ${INSTALLTMDIR}
    ######   Install Open MPI 4.1.0 with ucx support   
    # Starting from OpenMPI 4.0, UCX PML is preferred method for IB support
    # (https://www.open-mpi.org/software/ompi/major-changes.php)


    yum -y install numactl-devel
    ######   Install UCX
    # This setup has been obtained from: https://gitlab.com/ska-telescope/sdp/ska-sdp-exec-iotest/-/tree/master/singularity
    echo "UCX version: "
    cd ${INSTALLTMDIR}
    if [[ ! -f ${INSTALLTMDIR}/ucx-1.10.0.tar.gz ]] ; then
      wget https://github.com/openucx/ucx/releases/download/v1.10.0/ucx-1.10.0.tar.gz
    fi
    tar xzf ucx-1.10.0.tar.gz
    cd ucx-1.10.0
    # add this back to ucx_config_opts when available: --with-rdmacm  rdma/rdma_cma.h could not be found    
    # shared memory libs not installed: --with-knem=(DIR) --with-xpmem=(DIR)
    # Infiniband options: --with-rc --with-ud --with-dc  --with-mlx5-dv --with-ib-hw-tm --with-dm
    if [[ "$CMAKE_BUILD_TYPE" == "RelWithDebInfo" | "$CMAKE_BUILD_TYPE" == "Release" ]] ; then
      ucx_config_opts="--prefix=${INSTALL_PREFIX}/ucx --enable-optimizations --enable-mt --enable-cma --with-avx --with-march=native --with-dm  "
      ./contrib/configure-release ${ucx_config_opts}
    else
      ucx_config_opts="--prefix=${INSTALL_PREFIX}/ucx --enable-optimizations --enable-mt --enable-cma --with-dm "
      ./contrib/configure-devel ${ucx_config_opts}
    fi
    
    make -j$(NUMCORES) install

    ######   Install OpenMPI
    echo "OpenMPI version: ${OPENMPI_VERSION}"
    cd ${INSTALLTMDIR}
    # Download and unpack
    if [[ ! -f ${INSTALLTMDIR}/openmpi-${OPENMPI_VERSION}.tar.gz]] ; then
      wget -O openmpi-$OPENMPI_VERSION.tar.gz https://download.open-mpi.org/release/open-mpi/v$OPENMPI_VER_MAJ_MIN/openmpi-${OPENMPI_VERSION}.tar.gz
    fi
    tar -xf openmpi-$OPENMPI_VERSION.tar.gz
    cd openmpi-$OPENMPI_VERSION
    # Compile and install
    config_opts="--prefix=${INSTALL_PREFIX} --enable-orterun-prefix-by-default --enable-mpi-cxx --enable-static --enable-shared --with-ucx=${INSTALL_PREFIX}/ucx --without-verbs --enable-mca-no-build=btl-uct"

    # Set specific OpenMPI compiler flags
    export CFLAGS=${CFLAGS_OMPI}
    export CXXFLAGS=${CXXFLAGS_OMPI} 
    ./configure ${config_opts}
    make -j$(NUMCORES)
    make -j$(NUMCORES) install
    
        # set back flags:
    export CFLAGS=${CFLAGS_BUILD}
    export CXXFLAGS=${CFLAGS_BUILD}
    


    ###### Install OpenMPI
    # --with-libfabric=DIR    Deprecated synonym for --with-ofi
    # --with-libfabric-libdir=DIR
    #                         Deprecated synonym for --with-ofi-libdir
    # --with-ofi=DIR          Specify location of OFI libfabric installation,
    #                         adding DIR/include to the default search location
    #                         for libfabric headers, and DIR/lib or DIR/lib64 to
    #                         the default search location for libfabric libraries.
    #                         Error if libfabric support cannot be found.
    # --with-ofi-libdir=DIR   Search for OFI libfabric libraries in DIR
    # --with-ucx(=DIR)        Build with Unified Communication X library support
    # --with-ucx-libdir=DIR   Search for Unified Communication X libraries in DIR
    # --with-verbs(=DIR)      Build verbs support, optionally adding DIR/include,
    #                         DIR/lib, and DIR/lib64 to the search path for
    #                        headers and libraries
    # --with-verbs-libdir=DIR
    # cd ${INSTALLTMDIR}
    #wget --no-check-certificate https://download.open-mpi.org/release/open-mpi/v${OPENMPI_VER_MAJ_MIN}/openmpi-${OPENMPI_VERSION}.tar.gz
    # tar -xzf openmpi-${OPENMPI_VERSION}.tar.gz
    # cd openmpi-$OPENMPI_VERSION
    # # Do not use -march=native in flags in 4.1.0
    # # see: https://github.com/open-mpi/ompi/issues/8306  --enable-mca-no-build=op-avx
    # export CFLAGS="-O3 "
    # export CXXFLAGS="-O3 " 
    # ./configure --prefix=${INSTALL_PREFIX} --enable-shared 
    # make -j${NUMCORES}
    # make install

    # set back flags:
    export CFLAGS=$CMAKE_C_FLAGS
    export CXXFLAGS=$CMAKE_CXX_FLAGS  
   
  
%test    
 

%runscript
    

%startscript
    



%labels
    Author joshua-charles.bowden@inria.fr
    Version v0.0.1
