Bootstrap: localimage
From: /home/jbowden/ACROSS/WP7/container_opm/centos7debug



%help
    This container was built from the following repositpory of Singularity definition files:
        git clone https://gitlab.inria.fr/Damaris/Simulations/across_wp7_opmflow_scripts.git
        
    This version builds CentOS7 with updated gcc via RedHat devtoolset, Set DEVTOOLSET in both
    the %post and %environment sections below. Initialise the toolset using:
        > scl enable ${DEVTOOLSET} -- bash
        
    Most dependency versions can be define in : opm_damaris_stack_build_vars.sh which will
    be copied into the container in the first build step and sourced so as to get the version 
    numbers and install paths etc. Please modify the variables to update or modify the 
    exact dependencies wanted.

    |     | devtoolset | xerces-c | xsd   |  HDF5       |  UCX   |    mpi        | Paraview |
    |-----|------------|----------|-------|-------------|--------|---------------|----------|
    | src |            |          |       |             | 1.10.0 | OpenMPI 4.1.0 |          |
    | bin |  9         | 3.1.1    | 4.0.0 |             |        |               |          |
    
    This version installs DUNE git libraries from source
    (checkout version DUNETAG=v2.6.0)
    - libdune-common
    - libdune-geometry
    - libdune-istl   (apt dependencies libsuitespares-dev libsuperlu-dev)
    - libdune-uggrid-dev 
    - libdune-grid   (apt dependencies libalberta-dev)
    
    And OPM libraries from git src
    (checkout versionOPMTAG=release/2020.10)
    
    
    To build a contianer from a definition file:
       # Copy input variable file that will be sourced within the container
        # at the start of a build.
         cp across_wp7_opmflow_scripts/singularity/centos7/opm_damaris_stack_build_vars_debug.sh .
        # set the build variables in opm_damaris_stack_build_vars.sh to suite
        # Copy the definition files
        cp across_wp7_opmflow_scripts/singularity/centos7/centos7_mpi_src_debug.def .
        cp across_wp7_opmflow_scripts/singularity/centos7/opm_dune_mpi_srcbld_centos_debug_p2a.def .
        cp across_wp7_opmflow_scripts/singularity/centos7/opm_dune_mpi_srcbld_centos_debug_p2b.def .
        cp across_wp7_opmflow_scripts/singularity/centos7/opm_dune_mpi_srcbld_centos_debug_p2c.def .
      It now is a 4 stage build process:  - CentOS7 is old so we need to compile many again from source
        # step 1: Install some basic pre-packaged libraries - CentOS7 is old so we need to compile many again from source
        # also builds UCX and OpenMPI 4.1.0
        sudo singularity build --sandbox ./centos7debug centos7_mpi_src_debug.def
        # step 2: build Dune/OPM dependencies from source ( SuiteSparse SuperLU Vc TBB Scotch Zoltan etc.)     
        sudo singularity build --sandbox centos7debug_2a opm_dune_mpi_srcbld_centos_debug_p2a.def
        # step 3: build Damaris dependencies (Optional: HDF5, Paraview) and Damaris 
         sudo singularity build --sandbox centos7debug_2b opm_dune_mpi_srcbld_centos_debug_p2b.def
        # step 4: build Dune and OPM libraries  
        sudo singularity build --sandbox centos7debug_2c opm_dune_mpi_srcbld_centos_debug_p2c.def
        
    To get a shell into the container:
        sudo singularity shell --writable centos7debug_2c
                
    To locate the source repositories for dependencies and Damaris and OPM software:
        cd ${OMPBUILDSTACKDIR} # /opmbuildstackdir

        
%setup

%files


%environment


%post

    ############
    ### Enable newer gcc version 
    scl enable ${DEVTOOLSET} -- bash

    # Source the variables again
    source ${OMPBUILDSTACKDIR}/opm_stack_build_vars.sh
    

   ############
   ### Start the source package installs
   ############
    
   #### 
   ## This directory can be made in the files section above
   INSTALLTMDIR=${OMPBUILDSTACKDIR}
   # mkdir ${INSTALLTMDIR}
   

   cd ${INSTALLTMDIR}
   #### We have 3 DUNE prerequisite libraries, mostly used by dune-istl
   ## superlu, SuiteSparse and Vc
   ## The first two are also available as pre-built packages: libsuitesparse-dev libsuperlu-dev
   echo "SupeLU Version: ${SUPERLUTAG}"
   git clone --depth 1 --branch ${SUPERLUTAG}  https://github.com/xiaoyeli/superlu.git  
   repo=superlu
   cd ${INSTALLTMDIR}
   mkdir -p ./build/$repo
   cd ./build/$repo
   cmake ../../$repo -DCMAKE_INSTALL_PREFIX=${INSTALL_PREFIX} -DCMAKE_C_COMPILER=$CMAKE_C_COMPILER -DCMAKE_CXX_COMPILER=$CMAKE_CXX_COMPILER -DCMAKE_CXX_FLAGS="${CMAKE_CXX_FLAGS}" -DCMAKE_C_FLAGS="${CMAKE_C_FLAGS}" -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE} ${BUILD_SHARED} -DUSE_XSDK_DEFAULTS=TRUE -DTPL_BLAS_LIBRARIES="${SUPERLU_BLAS}" ${BUILD_SHARED} ${BUILD_TESTING}
    make -j${NUMCORES}
    make install
    
    # N.B. gmp tests passed with devtoolset-8
    # Dune can use the cxx version also 
    cd ${INSTALLTMDIR}
    echo "GMP Version: 6.2.1"
    wget https://gmplib.org/download/gmp/gmp-6.2.1.tar.xz
    tar -xf gmp-6.2.1.tar.xz
    cd gmp-6.2.1 
    ./configure --prefix=${INSTALL_PREFIX} --libdir=${INSTALL_PREFIX}/lib64 --includedir=${INSTALL_PREFIX}/include --enable-cxx
    make -j${NUMCORES}
    make install
    
    
    # This may be part of devtoolsset?
    cd ${INSTALLTMDIR}
    echo "MPFR Version: 4.1.0"
    wget https://www.mpfr.org/mpfr-current/mpfr-4.1.0.tar.gz
    tar -xf mpfr-4.1.0.tar.gz
    cd mpfr-4.1.0
    ./configure --prefix=${INSTALL_PREFIX} --libdir=${INSTALL_PREFIX}/lib64 --includedir=${INSTALL_PREFIX}/include
    make -j${NUMCORES}
    make install
    
       
    ###
    # Instal Vc
    echo "Vc Version: ${VC_TAG}"
    cd ${INSTALLTMDIR}
    git clone --depth 1 --branch $VC_TAG https://github.com/VcDevel/Vc.git
    cd Vc
    git submodule update --init
    repo=Vc
    mkdir -p ../build/$repo
    cd ../build/$repo
    cmake ../../$repo -DCMAKE_INSTALL_PREFIX=${INSTALL_PREFIX} -DCMAKE_C_COMPILER=$CMAKE_C_COMPILER -DCMAKE_CXX_COMPILER=$CMAKE_CXX_COMPILER  -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE} -DCMAKE_CXX_FLAGS="${CMAKE_CXX_FLAGS}" ${BUILD_SHARED}
    make -j${NUMCORES}
    make install
  

   # yum install ccache
   ###  # Fails on base CentOS7 as requires at least gcc v4.9. so devtoolset is required.
   #   N.B. SuiteSparse has a GPU implementation from NVida if using CHOLMOD function
   #   export CHOLMOD_USE_GPU=1”
   # SuiteSparse requires multi-precsionin libraries: gmp mpfr  (installed from source above)
   # SuitSparse installs a version of Metsis - A local Metsis install can be substituted if available (use MY_METSIS_LIB and MY_METSIS_INC variables)
   # N.B. from github readme: A recent OpenBLAS can result in severe performance degradation, use MKL BLAS
   # Check the README here: https://github.com/DrTimothyAldenDavis/SuiteSparse
   # Use "make distclean" if messing around between compiles and " make config [...]" to display paramaters being used
   cd ${INSTALLTMDIR}
   echo "SuiteSparse Version: ${SUITESPARSETAG}"
   git clone --depth 1 --branch ${SUITESPARSETAG} https://github.com/DrTimothyAldenDavis/SuiteSparse.git
   repo=SuiteSparse
   # cd ${INSTALLTMDIR}
   # mkdir -p ./build/$repo
   cd ./$repo
   AUTOCC=no CC=${GCC} CX=${GPP} JOBS=${NUMCORES} INSTALL=${INSTALL_PREFIX} make library BLAS="${BLAS_LIB}" LAPACK="${LAPACK_LIB}" CFOPENMP=${OPENMPLIB} OPTIMIZATION="${OPTIMIZATION}" 
   make install
   
   # N.B. INSTALL=${INSTALL_PREFIX} does not seem to work and libraries are placed in the build directory
   LD_LIBRARY_PATH=${OMPBUILDSTACKDIR}/SuiteSparse/lib:${OMPBUILDSTACKDIR}/SuiteSparse/lib64:$LD_LIBRARY_PATH

   
   
    cd ${INSTALLTMDIR}
    echo "TBB Version: tbb44u4"
    git clone --depth 1 --branch tbb44u4 https://github.com/wjakob/tbb.git
    repo=tbb
    mkdir -p ./build/$repo
    cd ./build/$repo
    cmake ../../$repo -DCMAKE_INSTALL_PREFIX=${INSTALL_PREFIX} -DCMAKE_C_COMPILER=${CMAKE_C_COMPILER} -DCMAKE_CXX_COMPILER=${CMAKE_CXX_COMPILER}  -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE} -DCMAKE_CXX_FLAGS=\"${CMAKE_CXX_FLAGS}\" ${BUILD_TESTING} ${BUILD_SHARED} ${TA}
    make -j$NUMCORES
    make install

   
  # PTScotch Install:  https://gitlab.inria.fr/scotch/scotch/-/blob/master/INSTALL.txt
   # This is a 32 bit integer index size (4 billion element mesh max size? or 4 billion vertecies?)
   # To find mpicc on CentOS7: 
   # export PATH=/usr/lib64/openmpi/bin:$PATH
   # /usr/include/openmpi-x86_64
   cd ${INSTALLTMDIR}
   echo "PTScotch Version: ${SCOTCH_TAG} "
   git clone --depth 1 --branch ${SCOTCH_TAG} https://gitlab.inria.fr/scotch/scotch.git
   repo=scotch
   cd $repo
   git submodule init
   git submodule update
   cd src
   ln -s Make.inc/Makefile.inc.x86-64_pc_linux2.shlib ./Makefile.inc
   
   # not available readily on CentOS7: -DCOMMON_FILE_COMPRESS_LZMA  
   # The threaded version of Scotch requires MPI thread support, which may not be the case for OPM
   # to enable set the following:
   #     SCOTCH_THREADS=4  # this is a hard coded number of threads that Scotch can use
   #     -DSCOTCH_PTHREAD -DSCOTCH_PTHREAD_NUMBER=${SCOTCH_THREADS} -DCOMMON_PTHREAD_AFFINITY_LINUX
   # N.B. "-DCOMMON_RANDOM_FIXED_SEED" is used by default
   # -DSCOTCH_METIS_VERSION=3 | 5
   # setting -DSCOTCH_METIS_VERSION=0, no MeTiS version will be exposed
   SCOTCH_SED=" -DIDXSIZE64 -I${MPI_INCDIR} -DINTSIZE32 -DSCOTCH_METIS_VERSION=5"
   sed -i "s|-DIDXSIZE64|${SCOTCH_SED}|" Makefile.inc
   # remove compressed gz file support as -lz is not added to dependent libs by default (i.e. dune-grid libs?)
   sed -i "s|-DCOMMON_FILE_COMPRESS_GZ||" Makefile.inc 
   
   find ./ -type f -name common.h -exec sed -i 's/MPI_INTEGER4/MPI_INTEGER/' {} \;  
   
   make libptscotch
   # on install the library ends up in a /lib directory, not lib64
   make prefix=${INSTALL_PREFIX} install 

    
   
    cd ${INSTALLTMDIR}
    # Zoltan is used by dune-alugrid
    # Zoltan (needs an MPI distribution and PT-Scotch or ParMetsis)
    # https://cs.sandia.gov/zoltan/ug_html/ug_usage.html
    echo "Zoltan Version: v3.83  "
    git clone --depth 1 --branch v3.83 https://github.com/sandialabs/Zoltan.git


    # MPI_INCDIR=/usr/local/include 
    cd ${INSTALLTMDIR}
    # --with-mpi-compilers to specify MPI compilers.
    # Or try --with-mpi-libs, --with-mpi-incdir=/usr/include/openmpi-x86_64 , --with-mpi-libdir=/usr/lib64/openmpi/lib     
    # --with-libdirs="/usr/local/lib64 /usr/local/lib"     --with-incdirs="/usr/local/include" 
    repo=Zoltan
    mkdir -p build/$repo
    cd build/$repo   
    ../../Zoltan/configure \
      --prefix=${INSTALL_PREFIX} \
      --with-gnumake \
      --with-scotch \
      --with-scotch-incdir="${INSTALL_PREFIX}/include" \
      --with-scotch-libdir="${INSTALL_PREFIX}/lib" \
      --with-id-type=uint \
      --with-libs=-lsuperlu \
      --with-ldflags="-L/usr/local/lib64 -L/usr/local/lib -L/usr/lib64 -L/usr/lib -L${INSTALL_PREFIX}/lib" \
      --enable-mpi \
      --with-mpi-libdir=${MPI_LIBDIR} \
      --with-mpi-incdir=${MPI_INCDIR} \
      --with-mpi-compilers=yes 
      
    # The --enable-gzip  flag build fails so do a manual addition of -lz    
    find ./ -type f -name Makefile -exec sed -i 's/ -lm$/ -lmpi_cxx  -lm -lz /' {} \; 
    # find ./ -type f -name Makefile -exec sed -i 's/ -lm / -lmpi_cxx  -lm -lz /' {} \;
    make everything
    make install

    
    
%test    
 

%runscript
    

%startscript
    



%labels
    Author joshua-charles.bowden@inria.fr
    Version v0.0.1
